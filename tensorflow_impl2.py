import util

import numpy as np

import tensorflow as tf

from tensorflow.keras.layers import Layer, InputLayer, Dense

from tensorflow.keras.models import load_model

import tensorflow_probability as tfp

import matplotlib.pyplot as plt



class Encoder(Layer):


  def __init__(self, D, hidden_layer_sizes):

    super(Encoder, self).__init__()


    latent_dim = hidden_layer_sizes[-1]


    # Create the encoder layers

    self.layers = tf.keras.Sequential()

    self.layers.add(InputLayer(input_shape=(D,)))

    for M_out in hidden_layer_sizes[:-1]:

      self.layers.add(Dense(M_out, activation="relu"))

    self.layers.add(Dense(2*latent_dim))


  def call(self, x):

    x = self.layers(x)

    mean, logvar = tf.split(x, num_or_size_splits=2, axis=1)

    return mean, logvar


class Decoder(Layer):


  def __init__(self, D, hidden_layer_sizes):

    super(Decoder, self).__init__()


    latent_dim = hidden_layer_sizes[-1]


    self.layers = tf.keras.Sequential()

    self.layers.add(InputLayer(input_shape=(latent_dim,)))

    for M_out in reversed(hidden_layer_sizes[:-1]):

      self.layers.add(Dense(M_out, activation="relu"))

    self.layers.add(Dense(D))


  def call(self, z, apply_sigmoid=False):

    logits = self.layers(z)

    if apply_sigmoid:

      probs = tf.sigmoid(logits)

      return probs

    return logits


class VariationalAutoEncoder(tf.keras.Model):

  def __init__(self, D, hidden_layer_sizes):

    super(VariationalAutoEncoder, self).__init__()

   

    self.latent_dim = hidden_layer_sizes[-1]


    self.encoder = Encoder(D, hidden_layer_sizes)

    self.decoder = Decoder(D, hidden_layer_sizes)


  def call(self, x, train=True):

    mean, logvar = self.encoder(x)

    z = self.sample_normal_dist(mean, logvar)

    x_logit = self.decoder(z)


    # Now add the ELBO loss

    if train:

        cross_ent = tf.nn.sigmoid_cross_entropy_with_logits(logits=x_logit, labels=x)

        logpx_z = -tf.reduce_sum(cross_ent, axis=[1])

        logpz = self.log_normal_pdf(z, 0., 0.)

        logqz_x = self.log_normal_pdf(z, mean, logvar)

        self.add_loss( -tf.reduce_mean(logpx_z + logpz - logqz_x) )


    return mean, logvar, z, x_logit


  def sample_normal_dist(self, mean, logvar):

    batch = tf.shape(mean)[0]

    dim = tf.shape(mean)[1]

    eps = tf.keras.backend.random_normal(shape=(batch, dim))

    #eps = tf.random.normal(shape=mean.shape)

    return eps * tf.exp(logvar * .5) + mean


  def log_normal_pdf(self, sample, mean, logvar, raxis=1):

    log2pi = tf.math.log(2. * np.pi)

    return tf.reduce_sum(

        -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),

        axis=raxis)


  def posterior_predictive_sample(self, x):

    # returns a sample from p(x_new | X)

    if len(x.shape) < 2:

      x = x.reshape(1,-1)

    mean, logvar, z, x_logits = self.call(x, train=False)

    return tfp.distributions.Bernoulli(logits=x_logits).sample()


  def prior_predictive_with_input(self, z):

    if len(z.shape) < 2:

      z = z.reshape(1,-1)

    return self.decoder(z.reshape(), apply_sigmoid=True)


  def prior_predictive_sample_with_probs(self):

    # returns a sample from p(x_new | z), z ~ N(0, 1)

    z =  tf.random.normal([self.latent_dim]).numpy().reshape(1,-1)

    logits = self.decoder(z)

    probs = tf.sigmoid(logits)

    return tfp.distributions.Bernoulli(logits=logits).sample(), probs




def main():


  X, Y = util.get_mnist()

  X = np.where(X > .5, 1.0, 0.0).astype('float32')


  vae = VariationalAutoEncoder(784, [200,100])


  try:

    vae.load_weights("vae_tf2_model").expect_partial()

    print("Loaded weights")

  except:

    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-4)

    vae.compile(optimizer=optimizer)

    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath="vae_tf2_model")

    vae.fit(X, X, batch_size=64, epochs=30,callbacks=[cp_callback])


 

  # plot reconstruction

  done = False

  while not done:

    i = np.random.choice(len(X))

    x = X[i]

    im = vae.posterior_predictive_sample(x).numpy().reshape(28, 28)

    plt.subplot(1,2,1)

    plt.imshow(x.reshape(28, 28), cmap='gray')

    plt.title("Original")

    plt.subplot(1,2,2)

    plt.imshow(im, cmap='gray')

    plt.title("Sampled")

    plt.show()


    ans = input("Generate another?")

    if ans and ans[0] in ('n' or 'N'):

      done = True


  # plot output from random samples in latent space

 

  done = False

  while not done:

    im, probs = vae.prior_predictive_sample_with_probs()

    im = im.numpy().reshape(28, 28)

    probs = probs.numpy().reshape(28, 28)

    plt.subplot(1,2,1)

    plt.imshow(im, cmap='gray')

    plt.title("Prior predictive sample")

    plt.subplot(1,2,2)

    plt.imshow(probs, cmap='gray')

    plt.title("Prior predictive probs")

    plt.show()


    ans = input("Generate another?")

    if ans and ans[0] in ('n' or 'N'):

      done = True

   


if __name__ == '__main__':

  main()