# imports
# util.py is local file helping us loading and normalizing the data
import util
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

st = None
try:
    st = tf.contrib.bayesflow.stochastic_tensor
except:
    # does not exist in later versins of TF
    # we will use the reparameterization trick instead
    # watch the later lecture on it
    pass    

Normal = tf.contrib.distributions.Normal
Bernoulli = tf.contrib.distributions.Bernoulli

# class in which a constructor is used to set weights, bias and activation function
class DenseLayer(object):
    def __init__(self, M1, M2, f=tf.nn.relu):

        self.W = tf.Variable(tf.random_normal(shape=(M1,M2)) * 2 / np.sqrt(M1))
        self.b = tf.Variable(np.zeros(M2).astype(np.float32))
        self.f = f

    def forward(self, X):
        """
        Forward operation: linear transformation and passing through activation function
        """
        return self.f(tf.matmul(X, self.W) + self.b)  

# most of the work is done in the constructor
# mean will have no activation
# standard deviation will have the softplus activation
class VariationalAutoencoder:
    def __init__(self, D, hidden_layer_sizes):
        # encoder => hidden_layer_size = size of every layer
        # up to the final hidden layer Z 
        # decoder => the decoder will have the reverse shape

        # represents a batch of training data
        self.X = tf.placeholder(tf.float32, shape=(None, D))

        # ENCODER
        # layers of the encoder
        # sizes of the layers of the decoder will just be the sizes of the encoder in reverse
        self.encoder_layers = []
        M_in = D
        for M_out in hidden_layer_sizes[:-1]:
            h = DenseLayer(M_in, M_out)
            self.encoder_layers.append(h)
            M_in = M_out

        # final encoder size= input of the decoder = M 
        M = hidden_layer_sizes[-1]

        # output size = 2 times as many units as specified by M_out
        # since needs to be M_out means + M_out variances
        h = DenseLayer(M_in, 2 * M, f=lambda x: x)
        self.encoder_layers.append(h)

        # getting the output of the encoder (mean + stddev = z)
        # separate them 
        # stddev = sigma
        # split on deviation to avoid getting a parameter too close to 0 and avoid singularity + using the softplus activation function 
        current_layer_value = self.X
        for layer in self.encoder_layer:
            current_layer_value = layer.forward(current_layer_value)
        self.means = current_layer_value[:, :M]
        self.stddev = tf.nn.softplus(current_layer_value[:, M:]) + 1e-6   

        # get a sample Z using a stochastic tensor
        # stochastic tensor
        # in order for the errors to be backpropagated past this point 
        if st is None:
            standard_normal = Normal(
                loc=np.zeros(M, dtype=np.float32),
                scale=np.ones(M, dtype=np.float32)
            )
            err = standard_normal.sample(tf.shape(self.means)[0])
            self.Z = err * self.stddev + self.means
        else:   
            with st.value_type(st.SampleValue()):
                self.Z = st.StochasticTensor(Normal(loc=self.means, scale=self.stddev))

        # DECODER
        # create a denselayer for each layer in reverse
        # since it is to be the opposite
        # the final layer will have the input dim
        self.decoder_layers = []
        M_in = M
        for M_out in reversed(hidden_layer_sizes[:-1]):
            h = DenseLayer(M_in, M_out)
            self.decoder_layers.append(h)
            M_in = M_out

        # final layer: no activation function is needed
        # Bernoulli accepts logits (pre-sigmoid)
        # using logits from Tensorflow instead of actual probabilities
        h = DenseLayer(M_in, D, f=lambda x: x)
        self.decoder_layers.append(h)

        # getting the logits
        current_layer_value = self.Z
        for layer in self.decoder_layers:
            current_layer_value = layer.forward(current_layer_value)
            logits = current_layer_value
            posterior_predictive_logits = logits 

        # get the Bernoulli distribution corresponding to the logits i.e. output
        self.X_hat_distribution = Bernoulli(logits=logits)

        # draw a sample from our distribution
        self.posterior_predictive = self.X_hat_distribution.sample()
        self.posterior_predictive_probs = tf.nn.sigmoid(logits)

        # draw our prior predictive sample
        # from a standard normale Z ~ N(0,1)
        standard_normal = Normal(
            loc=np.zeroes(M, dtype=np.float32),
            scale=np.ones(M, dtype=np.float32)
        )   

        # pass the sample through the decoder
        Z_std = standard_normal.sample(1)
        current_layer_value = Z_std
        for layer in self.decoder_layers:
            current_layer_value = layer.forward(current_layer_value)
        logits = current_layer_value

        prior_predictive_dist = Bernoulli(logits=logits)
        self.prior_predictive = prior_predictive_dist.sample()
        self.prior_predictive_probs = tf.nn.sigmoid(logits)

        # prior predictive from input
        # only used for generating visualization
        self.Z_input = tf.placeholder(tf.float32, shape=(None, M))
        current_layer_value = self.Z_input
        for layer in self.decoder_layers:
            current_layer_value = layer.forward(current_layer_value)
        logits = current_layer_value
        self.prior_predictive_from_input_probs = tf.nn.sigmoid(logits)

        # build the cost function
        # 2 parts
        # KL divergence between z and the earlier defined standard normal
        # summing the results
        if st is None:
            kl = -tf.log(self.stddev) + 0.5 * (self.stddev**2 + self.means**2) - 0.5
            kl = tf.reduce_sum(kl, axis=1)
        else:    
            kl = tf.reduce_sum(
                tf.contrib.distribution.kl_divergence(
                    self.Z.distribution, standard_normal
                ),
                1
            )
            expected_log_likelihood = tf.reduce_sum(
                self.X_hat_distribution.log_prob(self.X),
                1
            ) 

        # subtract the KL from the expected log likelihood
        self.elbo = tf.reduce_sum(expected_log_likelihood - kl)
        self.train_op = tf.train.RMSPropOptimizer(learning_rate=0.001).minimize(-self.elbo)

        # set up session and variables for later
        self.init_op = tf.global_variables_initializer()
        self.sess = tf.InteractiveSession()
        self.sess.run(self.init_op)

        # fit function
        # loop through the number of epochs
        # split the data into batches
        # call a train_op for each batch
        def fit(self, X, epochs=30, batch_size=64):
            costs = []
            n_batches = len(X) // batch_size
            print(f'n_batches is equal to : {n_batches}')
            for i in range(epochs):
                print(f'Epoch : {i}')
                np.random.shuffle(X)
                for j in range(n_batches):
                    batch = X[j * batch_size:(j+1) * batch_size]
                    _, c, = self.sess.run((self.train_op, self.elbo), feed_dict={self.X: batch})
                    c /= batch_size # for debugging
                    costs.append(c)
                    if j % 100 == 0:
                        print("iteration: %d, cost: %.3f" % (j, c))
            plt.plot(costs)
            plt.show()

        # transform function
        # map input X into corresponding latent vector Z
        def transform(self, X):
            return self.sess.run(
                self.means,
                feed_dict={self.X: X}
            )

        def prior_predictive_with_input(self, Z):
            return self.sess.run(
                self.prior_predictive_from_input_probs,
                feed_dict={self.Z_input: Z}
            )    

        def posterior_predictive_sample(self, X):
            """
            Return: sample from p(x_new | x)
            """ 
            return self.sess.run(self.posterior_predictive, feed_dict={self.X: X})  

        def prior_predictive_sample_with_probs(self):
            """
            returns a sample from p(x_new | z), z ~ N(0, 1)
            """
            return self.sess.run((self.prior_predictive, self.prior_predictive_probs))     

        # main function using the code above
        # get the data and convert it to actual binary variables (proper Bernoulli)
        # plot randomly selected data points from the training set
        # plot the original and reconstruction side by side
        # enter N to exit the loop     
        def main():
            X, Y = util.get_mnist()

            # convert X to binary variable
            X = (X > 0.5).astype(np.float32)

            vae = VariationalAutoencoder(784, [200, 100])
            vae.fit(X)

            # plot reconstruction
            done = False
            while not done:
                i = np.random.choice(len(X))   
                x = X[i]
                im = vae.posterior_predictive_sample([x]).reshape(28, 28)
                plt.subplot(1,2,2)
                plt.title("Original")
                plt.imshow(x.reshape(28,28), cmap='gray')
                plt.subplot(1,2,2)
                plt.imshow(im, cmap='gray')
                plt.title("Sampled")
                plt.show()

                user_answer = input("Generate another set?")
                if user_answer and user_answer[0] in ('n' or 'N'):
                    done = True

        # plot output from random samples in latent space
        # loop that generates prior predictive samples
        # this has not input because Z is sampled from the standard normal
        # side by side plot to look at means, and samples generated from it 
        # hit n to exit as well
            done = False
            while not done:
                im, probs = vae.prior_predictive_sample_with_probs()
                im = im.reshape(28, 28)
                probs = probs.reshape(28, 28)
                plt.subplot(1,2,1)
                plt.imshow(im, cmap='gray') 
                plt.title("Prior predictive sample")
                plt.subplot(1,2,2)
                plt.imshow(probs, cmap='gray')
                plt.title("Prior predictive probs")
                plt.show()

                user_answer = input("Generate another one?")
                if user_answer and user_answer[0] in ('n' or 'N'):
                    done = True

#if __name__ == '__main__':
#    main()     

# if the prior predictive samples generally look worse?
# Perhaps no actual images are mapped in the latent zone (digits confounded)