import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal as mvn

# softplus func 
def softplus(x):
    # automatically adds 1 to the argument
    # smooth and continuous
    return np.log1p(np.exp(x))

# neural network
# layer size (4, 3, 2)
# weight init
w1 = np.random.randn(4, 3)
w2 = np.random.randn(3, 2*2)

# 2*2 cuz we need 2 components/outputs for the mean and 2 for the standard deviation
# bias terms ignored for simplicity

# final output splitted to represent the mean and stddev
def forward(x, w1, w2):
    hidden = np.tanh(x.dot(w1))
    output = hidden.dot(w2)
    mean = output[:2]
    stddev = softplus(output[2:])
    return mean, stddev

# make a random input vector of size 4
x =np.random.rand(4)

# pass it through neural network to get mean and stddev
# get the parameters of the Gaussian
# this represent a model distribution i.e. q(z | x)
mean, stddev = forward(x, w1, w2)
print(f'mean: {mean}')
print(f'stddev: {stddev}')

# draw samples
# squarre stddev to make covariance along with a diagonal
samples = mvn.rvs(mean=mean, cov=stddev**2, size=10000)  

# plot the samples
# should display the mean and stddev calculated by the neural network
plt.scatter(samples[:,0], samples[:,1], alpha=0.5)  
plt.show()

